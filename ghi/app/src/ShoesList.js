import { useEffect, useState } from 'react';

function ShoesList() {
  const [shoes, setShoes] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/shoes/');

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes)
    }
}

  useEffect(()=>{
    getData()
  }, [])

  const handleDelete = async (id) => {
    const fetchOptions = {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(`http://localhost:8080/api/shoes/${id}`, fetchOptions);

    if (response.ok) {
      setShoes(shoes.filter(shoe => shoe.id !== id));
    } else {
      console.error('Failed to delete shoe');
    }
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Picture</th>
          <th>Model Name</th>
          <th>Manufacturer</th>
          <th>Color</th>
          <th>Bin</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map(shoe => {
          return (
            <tr key={shoe.href}>
                <td>
                <img src={shoe.pic_url} alt="Cool Shoes" class="img-thumbnail" style={{ width: 100, height: 150 }} />
                </td>
              <td>{ shoe.model_name }</td>
              <td>{ shoe.manufacturer }</td>
              <td>{ shoe.color }</td>
              <td>{ shoe.bin }</td>
              <td><button onClick={() => handleDelete(shoe.id)}>Delete</button></td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ShoesList;
