import React, {useState, useEffect} from 'react';

function ShoeForm() {

    // const [shoe, setShoe] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [model_name, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [pic_url, setPicUrl] = useState('');
    const [bins, setBins] = useState([]);
    const [bin, setBin] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.bin = bin;
        data.model_name = model_name;
        data.color = color;
        data.pic_url = pic_url;
        data.manufacturer = manufacturer;

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchOptions = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const shoeResponse = await fetch(shoeUrl, fetchOptions);
        if (shoeResponse.ok) {

          setBin('');
          setManufacturer('');
          setModelName('');
          setColor('');
          setPicUrl('');


        }
      }
      const handleChangeBin = (event) => {
        const value = event.target.value
        setBin(value);
      }

      const handleChangeManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
      }

      const handleChangeModelName = (event) => {
        const value = event.target.value;
        setModelName(value);
      }

      const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
      }

      const handleChangePicUrl = (event) => {
        const value = event.target.value;
        setPicUrl(value);
      }

        return (
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Create a new Shoe</h1>
                  <form onSubmit={handleSubmit} id="create-shoe-form">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeManufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                      <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeModelName} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
                      <label htmlFor="model_name">Model Name</label>
                    </div>
                    <div className="form-floating mb-3">
                      <textarea onChange={handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                      <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={handleChangePicUrl} placeholder="Picture" required type="text" name="pic_url" id="pic_url" className="form-control" />
                      <label htmlFor="pic_url">Picture</label>
                    </div>
                    <div className="mb-3">
                      <select value={bin} onChange={handleChangeBin} placeholder="Bin" name="bin" id="bin" className="form-select">
                        <option value="">Choose a bin</option>
                        {bins.map(bin => {
                          return (
                            <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
                          )
                        })}
                      </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                  </form>
                </div>
              </div>
            </div>
          );





}







export default ShoeForm;
