import React, {useEffect, useState } from 'react';

function HatForm() {
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [location, setLocation] = useState('')
    const [locations, setLocations] = useState([]);

  
    const fetchData = async () => {
      const url = 'http://localhost:8100/api/locations/';
  
      const response = await fetch(url);
  
      if (response.ok) {
        const data = await response.json();
        console.log({data})
        setLocations(data.locations);
      }
    }
  
    useEffect(() => {
      fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.fabric = fabric;
        data.style = style;
        data.color = color;
        data.location = location
        console.log(data)
        console.log(location)
  
    const url = `http://localhost:8090/api/hats/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);
    
    if (response.ok) {
        setFabric('');
        setStyle('');
        setColor('');
        setLocation('');
    }
  }
    
  const handleFabric = (event) => {
    const value = event.target.value;
    setFabric(value);
  }

  const handleStyle = (event) => {
    const value = event.target.value;
    setStyle(value);
  }

  const handleColor = (event) => {
    const value = event.target.value;
    setColor(value);
  }

  const handleLocation = (event) => {
    const value = event.target.value;
    setLocation(value);
  }
    
  return (
    <div className="row">
  <div className="offset-3 col-6">
    <div className="shadow p-4 mt-4">
      <h1>Create a new hat</h1>
      <form onSubmit={handleSubmit} id="create-hat-form">
        <div className="form-floating mb-3">
          <input value={fabric} onChange={handleFabric} placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control" />
          <label htmlFor="fabric">fabric</label>
        </div>
        <div className="form-floating mb-3">
          <input value={style} onChange={handleStyle} placeholder="style" required type="text" name="style" id="style" className="form-control" />
          <label htmlFor="style">style</label>
        </div>
        <div className="form-floating mb-3">
          <input value={color} onChange={handleColor} placeholder="color" required type="text" name="color" id="color" className="form-control" />
          <label htmlFor="color">color</label>
        </div>
        <div className="mb-3">
          <select value={location} onChange={handleLocation} required name="location" id="location" className="form-select">
            <option value="">Choose a location</option>
            {locations.map(location => {
                return (
                    <option key={location.id} value={location.href}>
                        {location.closet_name}
                    </option>
                )
            })}
          </select>
        </div>
        <button className="btn btn-primary">Create</button>
      </form>
    </div>
  </div>
</div>

  );
}

export default HatForm;
