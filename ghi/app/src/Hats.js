import React, { useEffect, useState } from 'react';

function HatList() {
  const [hats, setHats] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/hats");

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleDelete = async (id) => {
    const fetchOptions = {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
      },
    };
  
    const response = await fetch(`http://localhost:8090/api/hats/${id}`, fetchOptions);
  
    if (response.ok) {
      setHats(hats.filter(hat => hat.id !== id));
    }
  };


  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Fabric</th>
          <th>Style</th>
          <th>Color</th>
          <th>Picture URL</th>
          <th>Location</th>
          <th>Delete</th> 
        </tr>
      </thead>
      <tbody>
        {hats.map(hat => {
          return (
            <tr key={hat.id}>
              <td>{hat.fabric}</td>
              <td>{hat.style}</td>
              <td>{hat.color}</td>
              <td>{hat.picture_url}</td>
              <td>{hat.location}</td>
              <td>
                <button onClick={() => handleDelete(hat.id)}>Delete</button> 
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default HatList;
