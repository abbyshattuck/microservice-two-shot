# Wardrobify

Team:
* Abby - Shoes
* Henry - Hats

## Shoes microservice

-Models: created shoe model with properties outline in the requirments, and additionally created Value Object model Bin from Wardrobe.
-API: created views, functions for creating/viewing/deleting shoes and pulled data from Bin Foriegn key model
-Polling: created a polling function that continuously checked and updated Bin data from the Wardrobe microservice in order to provide access to all created bins for the shoe model

Frontend: created a functioning Create Shoe form that enabled the user to select available bins from a drop down box, and create a new shoe in the selected bin. Once a shoe was created, it was accurately reflected on the list shoe page with the picture image shown.

## Hats microservice
### Backend
- **Models**: Developed models based on the project requirements, including a detailed Hat model with attributes like style and color, stored in a PostgreSQL database.
- **API**: Implements API endpoints for creating, viewing, and deleting hats, serving as the main Hat microservice.
- **Polling**: Created a poll script to connect the Hat entity with the Location value object from the Wardrobe microservice, ensuring accurate hat and location details.

### Frontend
- **Hat Creation Form/List**: Built a React-based frontend interface showcasing a hat creation form and a list display of hats. This style allows for a simple way to create, view, and manage hats.
- **Integration with Backend**: Achieved integration between the frontend and backend, facilitating updates and interactions with the Hat microservice, ensuring the frontend reflects the current inventory status.
