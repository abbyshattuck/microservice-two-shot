from django.db import models
from django.urls import reverse

# Create your models here.
# fabric, style name, color, URL for pic, location in wardrobe
class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    section_number = models.PositiveSmallIntegerField(null=True) 

    def __str__(self):
        return self.closet_name



class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
        null=True
    )


    def __str__(self):
        return self.style

    def get_api_url(self):
        return reverse("api_list_hat", kwargs={"pk": self.pk})
